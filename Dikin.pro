#-------------------------------------------------
#
# Project created by QtCreator 2017-10-16T09:51:52
#
#-------------------------------------------------

TARGET = Dikin
TEMPLATE = app

SOURCES += \
        main.cpp \
        dikin.cpp

HEADERS += \
        dikin.h

QMAKE_CXXFLAGS += -O3 -std=c++11 -openmp -wall
LIBS += -openmp
