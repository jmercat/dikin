#include <iostream>
#include <time.h>
#include "dikin.h"

#include "eigen3/Eigen/Dense"

using namespace Eigen;

int main(int argc, char *argv[])
{
    initParallel();
    setNbThreads(4);
    srand((unsigned int) time(0));
    int m = 1000,n = 4000;
    MatrixXd A;
    VectorXd b;
    VectorXd c;

    A.resize(m,n);
    b.resize(m);
    c.resize(n);

    A.setRandom();
    b.setRandom();
    c.setRandom();
    c /= 2;
    c += VectorXd::Constant(n,0.5);

//    std::cout << "A " << A << std::endl << std::endl;
//    std::cout << "b " << b << std::endl << std::endl;
//    std::cout << "c " << c << std::endl << std::endl;



    Dikin dikin(A,b,c);

    clock_t t1,t2;
    t1 = clock();
    VectorXd x = dikin.Solve();
    t2 = clock();
    float computationTime = (float)(t2 - t1) / CLOCKS_PER_SEC;

    std::cout << (A*x - b).norm()/(A.norm()+b.norm()) << "  " << c.dot(x)/(c.norm()) << "  " << dikin.getNbIter() << std::endl;
    std::cout << "Computation time " << computationTime << "s" << std::endl;
    return 0;
}
