#include <iostream>
#include "dikin.h"
#include "eigen3/Eigen/IterativeLinearSolvers"
#include <omp.h>
#include "eigen3/Eigen/Core"

using namespace Eigen;

Dikin::Dikin(const MatrixXd &A, const VectorXd &b, const VectorXd& c, const VectorXd& x0):
mA(A),mb(b),mc(c),mx(x0),phase1Done(false),mFirstErr(-1)
{
    assert(mA.cols()>=mA.rows());

    std::cout << "Number of threads: " << nbThreads() << std::endl;
    if(mx.size() != mA.cols())
    {
        mx.resize(mA.cols());
        mCurrentErr = 1; //bigger value than acceptable error
    }else
    {
        mCurrentErr = (mA*mx - mb).norm()/(mA.norm() +mb.norm());
    }
    std::vector<int> negIndex = DikinNS::negArg(mx);
    isPossible = negIndex.size()==0;

    mnu = mx;
    mNbIter = 0;
}

Dikin::Dikin(const MatrixXd &A, const VectorXd &b, const VectorXd& c):
Dikin(A,b,c,VectorXd::Constant(A.cols(),0))
{}



Dikin::~Dikin()
{

}


void Dikin::Phase1()
{
    int m = mA.rows();
    int n = mA.cols();
    MatrixXd G(m,n+1);
    VectorXd h(m);
    VectorXd c(n+1);
    VectorXd z(n+1);

    G.block(0,0,m,n) = mA;
    h = -mA.rowwise().sum();
    G.col(n) = h;
    h += mb;

    c.setZero();
    c(n) =1;

    FullPivHouseholderQR<MatrixXd> qr;
    qr.compute(mA);
    mx = qr.solve(mb);

    double t = 2-mx.minCoeff();
    t = std::max(0.,t);
    z.head(n) = mx + VectorXd::Constant(n,t-1);
    z(n) = t;

    Dikin subDikin(G,h,c,z);
    subDikin.phase1Done = true;
    z = subDikin.Solve();
    mx = z.head(n) + VectorXd::Constant(n,1-z(n));
    isPossible = z(n)<1;
    phase1Done = true;
}

VectorXd Dikin::Solve(const double &err)
{
    if( mCurrentErr > err && !phase1Done)
        this->Phase1();
    if(!isPossible)
    {
        std::cout << "This cannot be solved, no vector verifying the conditions could be found" << std::endl;
        return mx;
    }else
    {
        mCurrentErr = err+1;
        while(mCurrentErr > err && isPossible)
            this->Iterate();
    }
    if(!isPossible)
        std::cout << "The problem is unbounded" << std::endl;
    return mx;
}

int Dikin::getNbIter() const
{
    return mNbIter;
}

void Dikin::Iterate()
{
    VectorXd xx = mx.cwiseProduct(mx);
    MatrixXd AHi = mA*xx.asDiagonal();

    //compute vector of dual variables
    LLT<MatrixXd> llt;
    llt.compute(AHi*mA.transpose());
    mnu = llt.solve(-AHi*mc);

    //compute vector of reduced costs
    VectorXd s = -(mc+mA.transpose()*mnu);

    isPossible = DikinNS::hasNegArg(mx.cwiseProduct(s));


    //compute the error for stop criteria
    this->ComputeError(s);
    isPossible = isPossible && (mCurrentErr<=100*mFirstErr);

    double mu = s.norm()*xx.norm();//sqrt(s.cwiseProduct(xx).dot(s));
    s = s.cwiseProduct(xx);


    //update x value
    s/=mu;
    mx += getStepSize(s)*s;
//    mx += s;

    mNbIter++;
//    std::cout << "                      " << (mA*mx - mb).norm()/(mA.norm()+mb.norm()) << "  " << mc.dot(mx)/mc.norm() << std::endl;
}

void Dikin::ComputeError(const VectorXd &s)
{
    mCurrentErr = fabs(mx.dot(s));
    if(mFirstErr == -1)
        mFirstErr = mCurrentErr;
//    std::cout << "Err :" << mCurrentErr << "  " << fabs((mc + mA.transpose()*mnu).minCoeff()) << std::endl;
}

double Dikin::getStepSize(const VectorXd &dx) const
{
    int ii = -1;
    double t;
    do
    {
        ii++;
        t = -mx(ii)/dx(ii);
    }while(dx(ii)>0);

    for(int i = ii; i< mx.size(); i++)
    {
        if(dx(i)<0)
        {
            t = std::min(t,-mx(i)/dx(i));
        }
    }
    return 0.95*t;
}

namespace DikinNS
{
    std::vector<int> negArg(VectorXd &x)
    {
        std::vector<int> output;
        for (int i = 0; i<x.size(); i++)
        {
            if(x(i)<0)
                output.push_back(i);
        }
//        std::cout << "n " << output.size() << std::endl;
        return output;
    }

    bool hasNegArg(const VectorXd& x)
    {
        for (int i = 0; i<x.size(); i++)
        {
            if(x(i)<0)
                return true;
        }
        return false;
    }
}

