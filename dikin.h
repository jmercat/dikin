#ifndef DIKIN_H
#define DIKIN_H

#include <vector>

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/Cholesky"

class Dikin
{

public:
    Dikin(const Eigen::MatrixXd& A,
          const Eigen::VectorXd& b,
          const Eigen::VectorXd& c, const Eigen::VectorXd &x0);
    Dikin(const Eigen::MatrixXd& A,
          const Eigen::VectorXd& b,
          const Eigen::VectorXd& c);
    ~Dikin();

    Eigen::VectorXd Solve(const double& err = 1.e-4);
    int getNbIter() const;
private:
    const Eigen::MatrixXd& mA;
    const Eigen::VectorXd& mb;
    const Eigen::VectorXd& mc;
    Eigen::VectorXd mnu;
    Eigen::VectorXd mx;
    double mCurrentErr;
    int mNbIter;
    bool isPossible;
    bool phase1Done;
    double mFirstErr;

    void Iterate();
    void ComputeError(const Eigen::VectorXd& s);
    double getStepSize(const Eigen::VectorXd& dx) const;
    void Phase1();

};

namespace DikinNS
{
   std::vector<int> negArg(Eigen::VectorXd &x);
   bool hasNegArg(const Eigen::VectorXd& x);
}

#endif // DIKIN_H
